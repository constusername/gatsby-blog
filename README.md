
## Setup

```
git clone https://gitlab.com/constusername/gatsby-blog.git blog
cd blog && yarn
yarn start
```

## Attributions

* [Gatsby](https://www.gatsbyjs.com/)
* [Gitlab](https://about.gitlab.com/)
* [Docker](https://www.docker.com/)
